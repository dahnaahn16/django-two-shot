from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def show_receipts(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "show_receipts": receipt,
    }
    return render(request, "receipts/show_receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.purchaser_id = request.user.id
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_receipt.html", context)


@login_required
def show_category(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "show_category": category,
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def show_account(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "show_account": account
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.owner_id = request.user.id
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "post_form": form
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.owner_id = request.user.id
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "post_form": form
    }
    return render(request, "receipts/create_account.html", context)
